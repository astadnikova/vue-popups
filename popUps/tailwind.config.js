/** @type {import('tailwindcss').Config} */
export default {
  mode: 'jit',
  content: [
    './src/components/**/*.{vue,js,ts}',
    './src/views/*.vue',
    './src/*.vue',
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#FF6363',
        'secondary': '#FFA63D',
      },

    },
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')],
}

